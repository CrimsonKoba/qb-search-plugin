const scriptURL =
  "https://corsproxy.io/?https://gitlab.com/CrimsonKoba/qb-search-plugin/-/raw/master/unionfansub.py";

async function getOriginalScript() {
  try {
    const response = await fetch(scriptURL);
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const script = response.text();
    const originalScript = script;
    return originalScript;
  } catch (error) {
    await showMessage("Error downloading the script file", true);
    console.error(error);
  }
}

async function showMessage(message, isError = false) {
  const messageDiv = document.getElementById("message");
  if (!messageDiv) return;
  messageDiv.textContent = message;
  messageDiv.className = isError ? "error" : "success";
}

function downloadScript(content, filename) {
  const blob = new Blob([content], { type: "text/plain" });
  const url = window.URL.createObjectURL(blob);
  const downloadLink = document.createElement("a");
  downloadLink.href = url;
  downloadLink.download = filename;
  downloadLink.style.display = "none";
  document.body.appendChild(downloadLink);
  downloadLink.click();
  window.URL.revokeObjectURL(url);
  document.body.removeChild(downloadLink);
}

async function processScript() {
  const username = document.getElementById("username").value.trim();
  const password = document.getElementById("password").value.trim();
  if (!username || !password) {
    await showMessage("Ingresa usuario y contraseña", true);
    return;
  }

  const script = await getOriginalScript();

  let modifiedScript = script
    .replace("USUARIO", username)
    .replace("CONTRASEÑA", password);

  const filename = "unionfansub.py";

  downloadScript(modifiedScript, filename);
  showMessage("Descarga completada");
}
